package ru.itis;

import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcImpl;

import java.sql.*;


public class Main {

    private static final String URL = "jdbc:postgresql://localhost:5432/summer_practice_day_1";
    private static final String USER = "postgres";
    private static final String PASSWORD = "green228";


    public static void main(String[] args) {
        SimpleDataSource dataSource = new SimpleDataSource();
        Connection connection = dataSource.openConnection(URL, USER, PASSWORD);
        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(connection);
        /*
        List<Student> sts = studentsRepository.findAllByAge(19);
        for (Student student : sts) {
            System.out.println(student.toString());
        }
        */
        Student s = new Student(4L,"Федор", "Достоевский", 27, 904);
        studentsRepository.update(s);
    }

}