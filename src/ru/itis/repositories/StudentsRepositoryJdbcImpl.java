package ru.itis.repositories;

import ru.itis.models.Mentor;
import ru.itis.models.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    private static final String SQL_SELECT_BY_ID = "select * from student where id = ";

    private static final String SQL_FIND_ALL = "SELECT s.*, m.first_name AS fname, m.last_name AS lname FROM student AS s " +
            "LEFT JOIN mentor AS m ON s.id = m.student_id";

    private static final String SQL_FIND_ALL_BY_AGE = "SELECT s.*, m.first_name AS fname, m.last_name AS lname FROM student AS s " +
            "LEFT JOIN mentor AS m ON s.id = m.student_id WHERE s.age = ";

    private static final String SQL_INSERT_STUDENT = "INSERT INTO student (first_name, last_name, age, group_number) VALUES (?, ?, ?, ?)";

    private static final String SQL_UPDATE_STUDENT = "UPDATE student SET (first_name, last_name, age, group_number) = (?, ?, ?, ?) " +
            "WHERE id = ?";

    private Connection connection;


    public StudentsRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Student> findAllByAge(int age) {
        Statement statement = null;
        ResultSet result = null;

        try {
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_FIND_ALL_BY_AGE + age);
            Map<Long, Student> students = new HashMap<>();

            while (result.next()) {
                Student student = new Student(
                        result.getLong("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getInt("age"),
                        result.getInt("group_number")
                );
                Mentor mentor = new Mentor(
                        result.getString("fname"),
                        result.getString("lname"),
                        student
                );
                if (!students.containsKey(student.getId())) {
                    students.put(student.getId(), student);
                    if (mentor.getFirstName() != null)
                        student.addMentor(mentor);
                } else {
                    students.get(student.getId()).addMentor(mentor);
                }
            }
            return new ArrayList<>(students.values());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    // Необходимо вытащить список всех студентов, при этом у каждого студента должен быть проставлен список менторов
    // у менторов в свою очередь ничего проставлять (кроме имени, фамилии, id не надо)
    // student1(id, firstName, ..., mentors = [{id, firstName, lastName, null}, {}, ), student2, student3
    // все сделать одним запросом
    @Override
    public List<Student> findAll() {
        Statement statement = null;
        ResultSet result = null;

        try {
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_FIND_ALL);
            Map<Long, Student> students = new HashMap<>();

            while (result.next()) {
                Student student = new Student(
                        result.getLong("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getInt("age"),
                        result.getInt("group_number")
                );
                Mentor mentor = new Mentor(
                        result.getString("fname"),
                        result.getString("lname"),
                        student
                );
                if (!students.containsKey(student.getId())) {
                    students.put(student.getId(), student);
                    if (mentor.getFirstName() != null)
                        student.addMentor(mentor);
                } else {
                    students.get(student.getId()).addMentor(mentor);
                }
            }
            return new ArrayList<>(students.values());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public Student findById(Long id) {
        Statement statement = null;
        ResultSet result = null;

        try {
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_SELECT_BY_ID + id);
            if (result.next()) {
                return new Student(
                        result.getLong("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getInt("age"),
                        result.getInt("group_number")
                );
            } else return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    // просто вызывается insert для сущности
    // student = Student(null, 'Марсель', 'Сидиков', 26, 915)
    // studentsRepository.save(student);
    // // student = Student(3, 'Марсель', 'Сидиков', 26, 915)
    @Override
    public void save(Student entity) {
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(SQL_INSERT_STUDENT, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setInt(3, entity.getAge());
            statement.setInt(4, entity.getGroupNumber());
            statement.executeUpdate();
            result = statement.getGeneratedKeys();
            if (result.next())
                entity.setId(result.getLong("id"));
            else
                throw new SQLException("id was not returned");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    // для сущности, у которой задан id выполнить обновление всех полей

    // student = Student(3, 'Марсель', 'Сидиков', 26, 915)
    // student.setFirstName("Игорь")
    // student.setLastName(null);
    // studentsRepository.update(student);
    // (3, 'Игорь', null, 26, 915)

    @Override
    public void update(Student entity) {
        PreparedStatement statement = null;
        try {
            if (entity.getId() == null || entity.getId() < 1)
                throw new SQLException("entity has invalid id");
            statement = connection.prepareStatement(SQL_UPDATE_STUDENT);
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setInt(3, entity.getAge());
            statement.setInt(4, entity.getGroupNumber());
            statement.setLong(5, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

}