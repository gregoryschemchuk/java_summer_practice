package ru.itis.models;


public class Mentor {

    private Long id;

    private String firstName;

    private String lastName;

    private Student student;


    public Mentor(String firstName, String lastName, Student student) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.student = student;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

}