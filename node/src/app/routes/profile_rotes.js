module.exports = function (app) {
    const fs = require('fs');

    app.get('/profile', (request, response) => {
        let result = {
            "name": "Gregory Schemchuk",
            "age": 18,
            "skills": "node.js, html css, jdbc"
        };
        response.setHeader("Content-Type", "application/json");
        response.send(JSON.stringify(result));
    });

    app.get('/getRecords', (request, response) => {
        fs.readFile('./app/routes/table.json', function read(err, data) {
            if (err) {
                throw err;
            }
            let result = JSON.parse(data);
            response.setHeader("Content-Type", "application/json");
            response.send(JSON.stringify(result));
        });
    });

    app.post('/doRecord', (request, response) => {
        let body = request.body;
        fs.readFile('./app/routes/table.json', 'utf8', function readFileCallback(err, data) {
            if (err) {
                console.log(err);
            } else {
                obj = JSON.parse(data);
                obj.table.push(body);
                json = JSON.stringify(obj);
                fs.writeFile('./app/routes/table.json', json, 'utf8', function(err) {
                    if (err) throw err;
                });
            }
        });
        response.redirect('/records.html');
    });
};